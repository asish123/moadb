import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempeform',
  templateUrl: './tempeform.component.html',
  styleUrls: ['./tempeform.component.css']
})
export class TempeformComponent implements OnInit {

  cities:object[] = [{id:1, name:'jerusalem'},{id:2, name:'London'},
                     {id:3, name:'Paris'}, {id:4, name:'non-exitent'}];
  temperature:number;
  city:string; 

  onSubmit(){
    this.router.navigate(['/temperatures', this.city]);
  }
  
  constructor(private router: Router) { }

  ngOnInit() {
  }


}
