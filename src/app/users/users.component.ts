import { Component, OnInit } from '@angular/core';
import { Users } from '../interfaces/users';
import { AuthService } from '../auth.service';
import { UsersService } from '../services/users.service';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
    Users$:Observable<Users[]>; 
    userId:string;
    usersId:number;
    
    constructor(private usersService: UsersService,private auth:AuthService,private db:AngularFirestore) { }
  
    ngOnInit() {
      this.usersService.getUsers()
      .subscribe(data =>this.Users$ = data );
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
         }
      )
    }

    add(usersId:number,usersEmail:string){
              this.usersService.addusers(this.userId,usersId,usersEmail)

}


}