import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  private errorMessage:string;

  constructor(private authService:AuthService,
    private router:Router) { 
      this.authService.getLoginErrors().subscribe(error => {
        this.errorMessage = error;
      });
    }

  email:string;
  password:string; 

  OnSubmitLogin(){
    this.authService.login(this.email,this.password);
    //this.router.navigate(['/welcome']);
  }

  ngOnInit() {
  }



}
