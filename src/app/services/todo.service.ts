import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

import { Observable } from 'rxjs';
import { Todo } from '../interfaces/todo';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  apitodo = "https://jsonplaceholder.typicode.com/todos"

  constructor(private http:HttpClient,private db:AngularFirestore){}
  
  userCollection:AngularFirestoreCollection = this.db.collection('users');
    todoCollection:AngularFirestoreCollection;
  
    //מוסיפה את הטודו הספציפי לקולקשיין
    addtodo(userId:string,title:string, completed:string){
      const todo = {title:title, completed:completed,};
      this.userCollection.doc(userId).collection('todos').add(todo);
    }

//לוקח מהאייפיאיי של טודו
getTodo():Observable<any>{
    return this.http.get<Todo[]>(this.apitodo);
  }

  gettodoofuser(userId):Observable<any[]>{
    this.todoCollection = this.db.collection(`users/${userId}/todos`);
        console.log('todo collection created');
        return this.todoCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

   //מוחקת טודו
   DeleteTodos(userId:string,id:string){
    this.db.doc(`users/${userId}/todos/${id}`).delete();
  }

  updatetodocheckbox(userId:string,id:string, completed:string){
     if(completed=="true"){
      this.db.doc(`users/${userId}/todos/${id}`).update(
        {
          completed:"false"
        }
      )

     }
     else{
 
      this.db.doc(`users/${userId}/todos/${id}`).update(
        {
          completed:"true"
        }
      )

     }
  }
}
