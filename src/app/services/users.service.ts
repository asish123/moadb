import { Injectable } from '@angular/core';
import { Users } from '../interfaces/users';
import { HttpClient } from '@angular/common/http';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiusers = "https://jsonplaceholder.typicode.com/users"

  
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  usersCollection:AngularFirestoreCollection;

  addusers(userId:string,usersId:number,usersEmail:string){
    const users = {usersId:usersId, usersEmail:usersEmail};
    this.userCollection.doc(userId).collection('users').add(users);
  }

  getUsers():Observable<any>{
    return this.http.get<Users[]>(this.apiusers);
  }

  getusersofuser(userId):Observable<any[]>{
    this.usersCollection = this.db.collection(`users/${userId}/users`);
        console.log('users collection created');
        return this.usersCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

  addtodos(userId:string,body:string, title:string){
    console.log('In add todos');
        const todo = {body:body, title:title};
        //this.db.collection('Posts').add(post);
        this.userCollection.doc(userId).collection('todo').add(todo);
      }
  constructor(private http: HttpClient,private db:AngularFirestore) { }

  // deleteuser(userId:string,id:string){
  //   this.db.doc(`users/${userId}/users/${id}`).delete();
  // }

}
