import { Injectable } from '@angular/core';
import { Post } from '../interfaces/post';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../interfaces/comment';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  apipost = "https://jsonplaceholder.typicode.com/posts"
  apicomment = "https://jsonplaceholder.typicode.com/comments"

  userCollection:AngularFirestoreCollection = this.db.collection('users');
    postCollection:AngularFirestoreCollection;
  
    //מוסיפה את הפוסט הספציפי לקולקשיין
    addpost(userId:string,title:string, body:string){
      const post = {title:title, body:body,like:0};
      this.userCollection.doc(userId).collection('posts').add(post);
    }

//לוקח מהאייפיאיי של פוסט
  getPost():Observable<any>{
    return this.http.get<Post[]>(this.apipost);
  }
//לוקח מהאייפיאיי של קומנט
  getcomment():Observable<any>{
    return this.http.get<Comment[]>(this.apicomment);
  }
// לוקח את הפוסטים של היוזרים בקולקשיין
  getpostofuser(userId):Observable<any[]>{
        this.postCollection = this.db.collection(`users/${userId}/posts`);
            console.log('Post collection created');
            return this.postCollection.snapshotChanges().pipe(
              map(actions => actions.map(a => {
                const data = a.payload.doc.data();
                data.id = a.payload.doc.id;
                return { ...data };
              }))
            ); 
      }
 // לוקחת פוסט מהקולקשין
      getonepost(userId, id:string):Observable<any>{
            return this.db.doc(`users/${userId}/posts/${id}`).get();
          }
//מוסיפה לייקים
   updatepostlike(userId:string, id:string,like:number){
        this.db.doc(`users/${userId}/posts/${id}`).update(
           {
             like:like
            }
            )
          }
          //מוחקת פוסטים
          deletePost(userId:string,id:string){
            this.db.doc(`users/${userId}/posts/${id}`).delete();
          }

         ///////////////////////// פונקציות של עריכה של הפוסט///////////////////////////

         //מעדכנת את הפוסט
         updatePost(userId:string,id:string,body:string,title:string,name:string){
          this.db.doc(`users/${userId}/posts/${id}`).update(
            {
              body:body,
              title:title,
              name:name
            }
          )
        }
        //לוקחת את הפוסט לעריכה
        getPostToEdit(userId,id:string):Observable<any>{
          return this.db.doc(`users/${userId}/posts/${id}`).get();
        }
 //מוסיפה פוסט לקולקשיין באמצעות כפתור הוספה
        addPosts(userId:string,body:string, title:string,name:string){
          console.log('In add posts');
              const post = {body:body, title:title, name:name,like:0};
              //this.db.collection('Posts').add(post);
              this.userCollection.doc(userId).collection('posts').add(post);
            }
       
  constructor(private http: HttpClient,private db:AngularFirestore) { }
}
