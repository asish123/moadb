import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {
  title:string;
  body:string;
  name:string;
  id:string;
  userId:string;
  btnText:string = "Add new post";
  isEdit:boolean = false;
  like:number;

  constructor(private postService: PostService, private router:ActivatedRoute,private Nroute:Router
    ,private authService:AuthService,) { }

  ngOnInit() {
    this.id = this.router.snapshot.params.id;
    this.authService.getUser().subscribe(
           user => {
            this.userId = user.uid;    
    if(this.id) {
    this.isEdit = true;
    this.btnText = 'Update post'   
    this.postService.getPostToEdit(this.userId,this.id).subscribe(
      post => {
        this.body = post.body;
        this.title = post.title; 
        this.name = post.name;
      }
    )}
    })
  }
     onSubmitAddPost(){
    if(this.isEdit){
      this.postService.updatePost(this.userId,this.id,this.body,this.title,this.name);
    } else {
     this.postService.addPosts( this.userId,this.body,this.title, this.name)
    }
    this.Nroute.navigate(['/savepost']);
  }

}
