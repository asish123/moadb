import { Component, OnInit } from '@angular/core';
import { ImageService } from '../services/image.service';
import { ClassifyService } from '../services/classify.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  category:string = "Loading...";
  categoryImage:string;

  constructor(public classifyService:ClassifyService,
              public imageService:ImageService) {}

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )
  }

}
