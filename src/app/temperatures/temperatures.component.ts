import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';
import { WeatherService } from '../services/temp.service';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  constructor(private route: ActivatedRoute, 
    private weatherservice:WeatherService) { }

likes = 0;
temperature;
image; 
city;
tempData$:Observable<Weather>;
errorMessage:string; 
hasError:boolean = false; 

addLikes(){
this.likes++
}
ngOnInit() {
//this.temperature = this.route.snapshot.params.temp;
this.city= this.route.snapshot.params.city;
this.tempData$ = this.weatherservice.searchWeatherData(this.city);
this.tempData$.subscribe(
data => {
console.log(data);
this.temperature = data.temperature;
this.image = data.image;
},
error =>{
console.log(error.message);
this.hasError = true; 
this.errorMessage = error.message; 
} 
)
}
}
