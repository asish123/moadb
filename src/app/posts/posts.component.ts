import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { AuthService } from '../auth.service';
import { Post } from '../interfaces/post';
import { Comment } from '../interfaces/comment';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  comments$:Observable<Comment[]>;
  Posts$:Observable<Post[]>;
  userId:string;
  text:string;
  postid:number;
  
  

  constructor(private postService: PostService,private auth:AuthService) { }

  add(title:string,body:string,id:number){
        this.postService.addpost(this.userId,title,body,)
        this.postid = id;
        this.text = "Saved for later viewing"
      }
  ngOnInit() {
    this.postService.getPost()
    .subscribe(data =>this.Posts$ = data );
    this.postService.getcomment()
   .subscribe(data =>this.comments$ = data );
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }

}
