import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoService } from '../services/todo.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-savedtodo',
  templateUrl: './savedtodo.component.html',
  styleUrls: ['./savedtodo.component.css']
})
export class SavedtodoComponent implements OnInit {

  todos$:Observable<any>;
  todoid:number;
  userId:string;
  completed:string;

  constructor(private todoService: TodoService,private auth:AuthService) { }

  ngOnInit() {
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
      this.todos$ = this.todoService.gettodoofuser(this.userId);
         }
      )
  }

  deletetodo(id:string){
    this.todoService.DeleteTodos(this.userId,id);
  }

  updatetodocheckbox(id:string, completed:string){
    
    return this.todoService.updatetodocheckbox(this.userId,id,completed);

  }

}
