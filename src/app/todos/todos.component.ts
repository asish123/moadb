import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Todo } from '../interfaces/todo';
import { AuthService } from '../auth.service';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  
  Todos$:Observable<Todo[]>;
  todoid:number;
  userId:string;
  completed:string;
  text:string;
  
  constructor(private todoService: TodoService,private auth:AuthService) { }

  add(title:string,id:number,completed:string){
        this.todoService.addtodo(this.userId,title,completed)
        this.todoid = id;
        this.text = "Saved for later viewing"
      }
  ngOnInit() {
    this.todoService.getTodo()
    .subscribe(data =>this.Todos$ = data );
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }
}
