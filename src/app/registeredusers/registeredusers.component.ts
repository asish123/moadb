import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-registeredusers',
  templateUrl: './registeredusers.component.html',
  styleUrls: ['./registeredusers.component.css']
})
export class RegisteredusersComponent implements OnInit {
  Users$:Observable<any>;
  userId:string;

  constructor(private usersService:UsersService,public auth:AuthService) { }

  ngOnInit() {
      //this.books$ = this.bookserv.getBooks();
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
      this.Users$ = this.usersService.getusersofuser(this.userId);
         }
      )
  }


  // deleteusers(id:string){
  //   this.usersService.deleteuser(this.userId,id)
  // }


}
