import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';

//Material
import {MatExpansionModule} from '@angular/material/expansion';
import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';

 //firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { HttpClientModule } from '@angular/common/http';
// import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { SavedpostComponent } from './savedpost/savedpost.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifyComponent } from './classify/classify.component';
import { PostformComponent } from './postform/postform.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { TempeformComponent } from './tempeform/tempeform.component';
import { TodosComponent } from './todos/todos.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { SavedtodoComponent } from './savedtodo/savedtodo.component';
import { UsersComponent } from './users/users.component';
import { RegisteredusersComponent } from './registeredusers/registeredusers.component';
import { UserTodoComponent } from './user-todo/user-todo.component';
import { TodoformComponent } from './todoform/todoform.component';


const appRoutes: Routes = [ 
  { path: 'welcome', component: WelcomeComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'allposts', component: PostsComponent },
  { path: 'savepost', component: SavedpostComponent },
  { path: 'classify', component: DocformComponent },
  { path: 'classified', component: ClassifyComponent },
  { path: 'postform', component: PostformComponent},
  { path: 'temperatures/:city', component: TemperaturesComponent},
  { path: 'tempform', component: TempeformComponent},
  { path: 'postform/:id', component: PostformComponent },
  { path: 'alltodos', component: TodosComponent},
  { path: 'savetodo', component: SavedtodoComponent},
  { path: 'users', component: UsersComponent},
  { path: 'registeredusers', component: RegisteredusersComponent},
  { path: 'userTodo', component: UserTodoComponent},
  { path: 'todoform', component: TodoformComponent},
  { path: '', redirectTo: '/welcome', pathMatch: 'full'}]; 
  
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    WelcomeComponent,
    PostsComponent,
    SavedpostComponent,
    DocformComponent,
    ClassifyComponent,
    PostformComponent,
    TemperaturesComponent,
    TempeformComponent,
    TodosComponent,
    SavedtodoComponent,
    UsersComponent,
    UserTodoComponent,
    TodoformComponent,
    RegisteredusersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
  RouterModule.forRoot( appRoutes,{ enableTracing: true }),
  AngularFireModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule, 
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule, 
    MatSliderModule,

    // CommonModule

  ],
  providers: [AngularFirestore,AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
