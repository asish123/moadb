// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCVMgKzxkOKIcsGpFmXmy9oBdQiisVxVKI",
    authDomain: "moadb2020-a489f.firebaseapp.com",
    databaseURL: "https://moadb2020-a489f.firebaseio.com",
    projectId: "moadb2020-a489f",
    storageBucket: "moadb2020-a489f.appspot.com",
    messagingSenderId: "205145817511",
    appId: "1:205145817511:web:b40048b285a20efedbb682"
}};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
